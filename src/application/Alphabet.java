package application;

public class Alphabet {
	public static String rotate(String str, int n) {
		String resultStr;
		String cutStr;
		String rotateStr;
		int strLength;
		
		strLength = str.length();
		if(n < 0) {
			n = strLength + n;
		}
		
		cutStr = str.substring(0, strLength - n);
		rotateStr = str.substring(strLength - n, strLength);
		resultStr = rotateStr + cutStr;
		return resultStr;
	}
}
