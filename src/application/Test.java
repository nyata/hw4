package application;

import static org.junit.Assert.*;

public class Test {

	@org.junit.Test
	public void test() {
		assertEquals(Alphabet.rotate("abcde",1), "eabcd");
		assertEquals(Alphabet.rotate("abcde",-2), "cdeab");
		assertEquals(Alphabet.rotate("abcde",3), "cdeab");
		assertEquals(Alphabet.rotate("abcde",-4), "eabcd");

	}

}
